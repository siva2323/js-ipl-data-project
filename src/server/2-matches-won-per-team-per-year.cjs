const fs=require('fs')
const matches = require('../data/matches.json'); 


function matchesWonPerTeamPerYear (matches)
{
if((matches=== undefined)||(!Array.isArray(matches))) return {};

let matchesWon=matches.reduce((accumulator,match) =>          
{
    if (match["winner"] in accumulator) {
        
        if (match["season"] in accumulator[match["winner"]]) {
          accumulator[match["winner"]][match["season"]] += 1;
        } else {
          accumulator[match["winner"]][match["season"]] = 1;
        }
      } else {
        accumulator[match["winner"]] = {};
        accumulator[match["winner"]][match["season"]] = 1;
      }

      
    return accumulator;
},{});


return matchesWon;

}


module.exports=matchesWonPerTeamPerYear;



fs.writeFileSync("../public/output/2-matches-won-per-team-per-year.json",JSON.stringify(matchesWonPerTeamPerYear(matches)),'utf8',
function(err){console.log(err);});
