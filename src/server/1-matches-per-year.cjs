const fs=require('fs')
const matches = require('../data/matches.json'); 


function matchesPerYear(matches)
{

  if(matches === undefined ||   !Array.isArray(matches)) return {};


  
  let matchArray=matches.reduce((accumulator,match) =>
  {
    if(match["season"] in accumulator)
    {
      accumulator[match["season"]] +=1;
    }else 
    {
      accumulator[match["season"]]=1;
    }
    return accumulator;
  }, {});

  
  return matchArray;

   
}
module.exports=matchesPerYear;



fs.writeFileSync("../public/output/1-matches-per-year.json",JSON.stringify(matchesPerYear(matches)),'utf8',
function(err){console.log(err);});


