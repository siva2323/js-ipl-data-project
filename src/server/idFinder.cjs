const deliveries = require('../data/deliveries.json'); 
const matches = require('../data/matches.json'); 


let yearsToFind=(year) =>
{
    let idArray=[];

    for(let loop=0;loop<matches.length;loop++)
    {
        let temp=matches[loop];
        
        if(temp["season"] == year)
        {
            let convert=Number(temp["id"]);
            idArray.push(convert);
        }
        
    }
    startId=idArray[0];
    endId=idArray[idArray.length-1];
    return [startId,endId];
}


module.exports=yearsToFind;


