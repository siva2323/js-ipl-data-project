const fs=require('fs');
const deliveries = require('../data/deliveries.json'); 
const yearsToFind=require('./idFinder.cjs');


 const currentBatsman="V Kohli";
 let balls=0;

 function strikeRate(deliveries)
 {

    let answer=["Batsman : "+currentBatsman];
    for(let year=2008;year<2018;year++)
    {
    let startToEnd= yearsToFind(year);
    let startId=startToEnd[0];
    let endId=startToEnd[1];
    if(deliveries === undefined ||   !Array.isArray(deliveries)) return [];

    let sum=deliveries.reduce((totalRuns,match) =>
    {
        if((match["match_id"]>= startId)&&(match["match_id"]<=endId))
        {

        if(match["batsman"] === currentBatsman)
        {
            balls++;
            totalRuns +=Number(match["batsman_runs"]);
            
        }
        }
        return totalRuns;

    },0);
    let temp=(sum/balls)*100;
    balls=0;
    
    answer.push(year +" : "+temp.toFixed(2));
    }
    return answer;
 }
 

module.exports=strikeRate;


fs.writeFileSync("../public/output/7-strike-rate-of-a-batsman.json",JSON.stringify(strikeRate(deliveries)),'utf8',
function(err){console.log(err);});
