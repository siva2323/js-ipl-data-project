const fs=require('fs');
const deliveries = require('../data/deliveries.json'); 



function bestEconomyBowler(deliveries)
{

    if(deliveries === undefined ||   !Array.isArray(deliveries)) return [];
let bowlers=deliveries.reduce((accumulator,match) =>
{
    if(match["is_super_over"] !== "0")
    {

        
        if(match["bowler"] in accumulator)
        {
            accumulator[match["bowler"]]["super_over"] +=1;
           
            accumulator[match["bowler"]]["totalRuns"] +=Number(match["total_runs"]);
            
        }else 
        {
            
            accumulator[match["bowler"]]={"totalRuns":+match["total_runs"],"super_over":1};
        }
    }
    return accumulator;
    

},{});

   let economyArray={}
    for (let player in bowlers )
    {
        let runs=(bowlers[player]["totalRuns"]);
        let over=bowlers[player]["super_over"];
        let economyRate=runs/over;
        economyRate=economyRate.toFixed(2);
        
        economyArray[player]=[economyRate];

    }
    
    
    let bestBowler=Object.entries(economyArray).sort((a,b) =>
    {
        
        if(a[1]<b[1]) return -1;
        else return 1;

    });
    
    return bestBowler[0];

}



module.exports=bestEconomyBowler;

fs.writeFileSync("../public/output/9-bowler-with-best-economy-in-super-over.json",JSON.stringify(bestEconomyBowler(deliveries)),'utf8',
function(err){console.log(err);});



