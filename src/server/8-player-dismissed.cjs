const fs=require('fs')
const deliveries = require('../data/deliveries.json'); 



function highestDismissedPlayer(deliveries)
{
    
    if(deliveries === undefined ||   !Array.isArray(deliveries)) return [];
    let playerDismissed=deliveries.reduce((accumulator,match) =>
    {
        
        if(match["player_dismissed"] in accumulator)
        {
            accumulator[match["player_dismissed"]] +=1;


        }else 
        {
            if(match["player_dismissed"] !== "")
            {
            accumulator[match["player_dismissed"]]=1;
            }
        }
    return accumulator;

    },{});

        let playerDismissedArray=Object.entries(playerDismissed);
        let highestDismissed=playerDismissedArray.sort(
            (player1,player2) =>
        {
            if(player1[1]>player2[1])
            {
                return -1;
            }
            else return 1;

        });
        return highestDismissed[0];
}

module.exports=highestDismissedPlayer;


fs.writeFileSync("../public/output/8-player-dismissed.json",JSON.stringify(highestDismissedPlayer(deliveries)),'utf8',
function(err){console.log(err);});
