const fs=require('fs');
const matches=require('../data/matches.json');

function playerOftheMatch(matches)
{
    if((matches=== undefined)||(!Array.isArray(matches))) return {};

    let listOfPlayers=matches.reduce((accumulator,match) =>
    {
        if(match["season"] in accumulator)
        {
            if(match["player_of_match"] in accumulator[match["season"]])
            {
                accumulator[match["season"]][match["player_of_match"]] +=1;
            } else 
            {
                accumulator[match["season"]][match["player_of_match"]]=1;
            }

        }else 
        {
            accumulator[match["season"]]={};
            accumulator[match["season"]][match["player_of_match"]]=1;

        }
        return accumulator;

    },{})
    let playerOfTheMatchInManyMatches = Object.entries(listOfPlayers).map((perYear) => 
        {
          
          let playerlist = Object.entries(perYear[1]).sort((a,b)  => b[1]-a[1]);
          return [perYear[0], playerlist[0]];
        });
      playerOfTheMatchInManyMatches = Object.fromEntries(playerOfTheMatchInManyMatches);
      return playerOfTheMatchInManyMatches;
      
    


}


module.exports=playerOftheMatch;

fs.writeFileSync("../public/output/6-player-of-the-match.json",JSON.stringify(playerOftheMatch(matches)),'utf8',
function(err){console.log(err);});
