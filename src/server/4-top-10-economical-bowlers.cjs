const fs=require('fs');
const deliveries = require('../data/deliveries.json'); 
const yearsToFind=require('./idFinder.cjs');



function top10EconomicalBowlers(deliveries,year)
{
    let yearId=yearsToFind(year);
    if((deliveries === undefined)||(!Array.isArray(deliveries))) return [];
    
    let bowlers=deliveries.reduce((accumulator,match) =>
    {
        if((match["match_id"]>=yearId[0])&&(match["match_id"])<=yearId[1])
        {
            
            if(match["bowler"] in accumulator)
            {
                accumulator[match["bowler"]]["runs"] +=Number(match["total_runs"]);
                accumulator[match["bowler"]]["balls"] +=1;

            }else 
            {
                accumulator[match["bowler"]]={"runs":+match["total_runs"],"balls":1};
                
            }
        }
        return accumulator;
    },{});


    let topListBowlers={};
    for(let player in bowlers)
    {
        let runs=bowlers[player]["runs"];
        let over=bowlers[player]["balls"];
        over =Math.floor(over/6);
        let economyRate=(runs/over);
        economyRate=economyRate.toFixed(2);
        topListBowlers[player]=[economyRate];
    }

    
    let topTen=Object.entries(topListBowlers).sort((a,b) => a[1]-b[1] );

    return topTen.slice(0,10);
}


module.exports=top10EconomicalBowlers;

fs.writeFileSync("../public/output/4-top-economical-bowlers.json",JSON.stringify(top10EconomicalBowlers(deliveries,2015)),'utf8',
function(err){console.log(err);});


