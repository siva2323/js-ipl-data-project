const fs=require('fs')
const matches = require('../data/matches.json'); 

let teamsWonTossAndMatch=matches =>
{
    if((matches=== undefined)||(!Array.isArray(matches))) return {};
    let teamsWon=matches.reduce((accumulator,match) =>
    {   
        if(match["toss_winner"] === match["winner"])
        {
            if(match["winner"] in accumulator)
            {
                accumulator[match["winner"]] +=1;


            }else 
            {
                accumulator[match["winner"]]=1;
            }

        }
        return accumulator;
    },{});
    return teamsWon;
}

module.exports=teamsWonTossAndMatch;


fs.writeFileSync("../public/output/5-teamsWonTossAndMatch.json",JSON.stringify(teamsWonTossAndMatch(matches)),'utf8',
function(err){console.log(err);});
