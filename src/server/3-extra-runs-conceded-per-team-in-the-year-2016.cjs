const fs=require('fs')
const deliveries = require('../data/deliveries.json'); 
const yearsToFind=require('./idFinder.cjs');




let extraRunsPerYear =(deliveries,year) =>
{
    let arr=yearsToFind(year);
    if(deliveries === undefined ||  !Array.isArray(deliveries)) return [];
    let extraRuns=deliveries.reduce((accumulator,match) => 
    {
    if((match["match_id"]>= arr[0])&&(match["match_id"]<=arr[1]))
    {   
        if(Number(match["extra_runs"])>0)
        {
            
            if(match["bowling_team"] in accumulator)
            {
                accumulator[match["bowling_team"]] +=Number(match["extra_runs"]);
            }else 
            {
                accumulator[match["bowling_team"]]=Number(match["extra_runs"]);
                
            }
        
        }
    }
        return accumulator;
    },{});
    return extraRuns;

}


module.exports=extraRunsPerYear;


fs.writeFileSync("../public/output/3-Extra-runs-conceded-per-team-in-the-year-2016.json",JSON.stringify(extraRunsPerYear(deliveries,2016)),'utf8',
function(err){console.log(err);});
